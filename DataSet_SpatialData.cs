﻿using System.Data;

namespace Jeforz.Common
{
    public partial class DataSet_SpatialData
    {
        private Spatial_Query_BlockType1DataTable m_pBlockType1DataTable = new Spatial_Query_BlockType1DataTable();

        partial class Spatial_Query_LineType1DataTable
        {
        }
    
        private static Storage_Engine m_pStorageEngine = null;

        public static Storage_Engine StorageEngine { set { m_pStorageEngine = value; } get { return m_pStorageEngine; } }

        public bool GetObjectRowFromUniqueID(object i_iUniqueID, MapObject.Type i_iType, Database i_iDatabase, bool i_bForceFill, out DataRow o_pDataRow, bool i_bUseWater = false)
        {
            DataTable l_pDataTable = null;
            DataRow l_pDataRow;
            string l_sCriteria;
            bool l_bReturn = false;

            o_pDataRow = null;

            if (i_iUniqueID == null) return false;

            // 13.10.02 MAL : added additional Point_ViewID criteria for ComplexBlocks

            l_sCriteria = "UniqueID = " + "'" + i_iUniqueID + "'";
            if (i_iType == MapObject.Type.ComplexBlock) l_sCriteria += " AND " + "((Point_ViewID Is Null) OR (Point_ViewID = 1))";

            // 09/12/14 FirasI the following line check if uniqueId is 0 then no object to return
            // 12.04.13 MAL : changed to chec if UniqueID is non-zero

            if (!Utility_ID.IsNullOrEmpty(i_iUniqueID))
            {
                switch (i_iType)
                {
                    case MapObject.Type.ComplexBlock: 

                        if (Utility_ID.UseUniqueIdentifiers && (i_iDatabase == Database.SpatialV1))
                        {
                            // 13.10.28 MAL : force use of a separate DataTable to prevent mixture of INT and UniqueIdentifier UniqueIDs

                            l_pDataTable = m_pBlockType1DataTable;
                        }
                        else
                        {
                            l_pDataTable = this.Spatial_Query_BlockType1;
                        }

                        break;

                    case MapObject.Type.ComplexLine: l_pDataTable = this.Spatial_Query_LineType1; break;
                    case MapObject.Type.ComplexText: l_pDataTable = this.Spatial_Query_TextType1; break;
                    case MapObject.Type.SimpleBlock: l_pDataTable = this.Spatial_Query_BlockType2; break;
                    case MapObject.Type.SimpleLine: l_pDataTable = this.Spatial_Query_LineType2; break;
                    case MapObject.Type.SimpleText: l_pDataTable = this.Spatial_Query_TextType2; break;
                }

                if (Storage_Engine.TryGetDataRow(l_pDataTable, l_sCriteria, out l_pDataRow))
                {
                    o_pDataRow = l_pDataRow;

                    if (i_bForceFill)
                    {
                        // if ForceFill is set, clear the UniqueID of the DataRow and continue on to select directly from the database

                        o_pDataRow["UniqueID"] = null;
                    }
                    else
                    {
                        l_bReturn = true;
                    }
                }

                if (!l_bReturn)
                {
                    // if it does not exist, force fill the row from the database
                    
                    Database_Command_SQL l_pCommand = new Database_Command_SQL();

                    l_pCommand.Database = i_iDatabase;

                    if (i_iDatabase == Database.SpatialV1)
                    {
                        l_pCommand.AppendCommandString(Database_Command.StringType.Fields, l_pCommand.GetFieldsFromDataTable(l_pDataTable).Replace(",[LineString]", ""), "");
                        l_pCommand.OverrideFieldList = true;
                    }


                    if (i_bUseWater && i_iType == MapObject.Type.ComplexBlock)
                    {
                        //2015.10.29 AndrewY- Hartford Water
                        // Spatial_Query_BlockType1 now filters out water blocks.  All water blocks are now drawn from Spatial_Query_BlockType1Water.
                        // override Spatial_Query_BlockType1 so that creating design on existing water network correctly recognizes the block.
                        l_pCommand.SetCommandString(Database_Command.StringType.Source, "Spatial_Query_BlockType1Water" );
                    }

                    l_pCommand.AppendCommandString(Database_Command.StringType.Criteria, l_sCriteria, " AND ");

                    StorageEngine.FillDataTable(l_pDataTable, l_pCommand, true, false);
                                        
                    if (Storage_Engine.TryGetDataRow(l_pDataTable, l_sCriteria, out l_pDataRow)) o_pDataRow = l_pDataRow;
                }
            }

            return (o_pDataRow != null);
        }
    }
}
