﻿using System.Data;

namespace Jeforz.Common
{
    partial class DataSet_Spatial
    {
        partial class Spatial_Query_SystemObjectByTypeDataTable
        {
        }
    
        public bool GetObjectRowFromUniqueID(int i_iUniqueID, MapObject.Type i_iType, out DataRow o_pDataRow)
        {
            DataTable l_pDataTable = null;
            DataRow l_pDataRow;
            string l_sCriteria = ("UniqueID" + "=" + i_iUniqueID);

            o_pDataRow = null;

            switch (i_iType)
            {
                case MapObject.Type.ComplexBlock: l_pDataTable = this.Spatial_Query_BlockType1; break;
                case MapObject.Type.ComplexLine: l_pDataTable = this.Spatial_Query_LineType1; break;
                case MapObject.Type.ComplexText: l_pDataTable = this.Spatial_Query_TextType1; break;
                case MapObject.Type.SimpleBlock: l_pDataTable = this.Spatial_Query_BlockType2; break;
                case MapObject.Type.SimpleLine: l_pDataTable = this.Spatial_Query_LineType2; break;
                case MapObject.Type.SimpleText: l_pDataTable = this.Spatial_Query_TextType2; break;
            }

            if (Storage_Engine.TryGetDataRow(l_pDataTable, l_sCriteria, out l_pDataRow))
            {
                o_pDataRow = l_pDataRow;
            }
            else
            {
                // if it does not exist, force fill the row from the database

                Storage_Engine_Factory.Engine.FillDataTableByCriteria(l_pDataTable, Database.Spatial, ("UniqueID" + "=" + i_iUniqueID), false);

                if (Storage_Engine.TryGetDataRow(l_pDataTable, l_sCriteria, out l_pDataRow)) o_pDataRow = l_pDataRow;
            }

            return (o_pDataRow != null);
        }

        public bool GetSystemObjectRowFromTypeID(int i_iTypeID, out Spatial_Query_SystemObjectByTypeRow o_pDataRow)
        {
            DataRow l_pDataRow;

            o_pDataRow = null;

            if (Storage_Engine.TryGetDataRow((DataTable)this.Spatial_Query_SystemObjectByType, ("UniqueID" + "=" + i_iTypeID), out l_pDataRow)) o_pDataRow = (Spatial_Query_SystemObjectByTypeRow)l_pDataRow;

            return (o_pDataRow != null);
        }
    }
}
