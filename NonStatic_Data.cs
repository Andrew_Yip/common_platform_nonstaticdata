﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jeforz.Common
{
    // FirasI 2009/06/04 Created 

    public class NonStatic_Data : NonStatic_Data_Base
    {
        private DataSet_Redline m_pDataSet_Redline = null;
        private DataSet_Attribute m_pDataSet_Attribute = null;
        private DataSet_ReportSpatial m_pDataSet_ReportSpatial = null;
        private DataSet_SpatialData m_pDataSet_SpatialData = null;
        private DataSet_SpatialSnapshot m_pDataSet_SpatialSnapshot = null;        
        private DataSet_JointUse m_pDataSet_JointUse = null;
        private DataSet_LegacyInventory m_pDataSet_LegacyInventory = null;
        private DataSet_Outage m_pDataSet_Outage;
        private DataSet_LegacyWorkOrder m_pDataSet_LegacyWorkOrder = null;
        private DataSet_SpatialSnapshot_V2_Type2 m_pDataSet_SpatialSnapshot_V2_Type2 = null;
        private DataSet_FusionSpatial m_pDataSet_SpatialSnapshot_V3_Type2 = null;
        private DataSet_ReportWorkOrder m_pDataSet_ReportWorkOrder = null;
        private static DataSet_Gallery m_pDataSet_Gallery = null;
        private DataSet_ReportTimeTracker m_pDataSet_ReportTimeTracker = null;
        private DataSet_CubeCity m_pDataSet_CubeCity = null;

        private DataSet_ReportSpatialV3 m_pDataSet_ReportSpatialV3 = null;
        private DataSet_ReportOutageV3 m_pDataSet_ReportOutageV3 = null;

        private DataSet_Datapump m_pDataSet_Datapump = null; // 2017.04.27 AliE - Multispeak tables

        public NonStatic_Data(System_Engine_Base i_pSystemEngine) : base(i_pSystemEngine)
        {

        }

        public DataSet_Gallery Gallery
        {
            get
            {
                if (m_pDataSet_Gallery == null)
                {
                    m_pDataSet_Gallery = new DataSet_Gallery();

                    // SystemEngine.StorageEngine.SetDataSet(m_pDataSet_Gallery);
                    //SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Gallery.Spatial_Attribute_Document, Database.Spatial, null, true);
                }

                return m_pDataSet_Gallery;
            }
        }
        
        //PouyaS 10/04/07

        public DataSet_LegacyWorkOrder LegacyWorkOrder
        {
            get
            {
                if (m_pDataSet_LegacyWorkOrder == null)
                {
                    m_pDataSet_LegacyWorkOrder = new DataSet_LegacyWorkOrder();

                    // SystemEngine.StorageEngine.SetDataSet(m_pDataSet_LegacyWorkOrder);

                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_LegacyWorkOrder.WO_Application_Master, Database.WorkOrder, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_LegacyWorkOrder.WO_Request_Master, Database.WorkOrder, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_LegacyWorkOrder.Klickitat_Request_Customer, Database.Client, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_LegacyWorkOrder.Lakeview_Request_Master, Database.Client, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_LegacyWorkOrder.Klickitat_Request_Address, Database.Client, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_LegacyWorkOrder.System_User_Master, Database.Fusion, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_LegacyWorkOrder.System_Attribute_State, Database.Client, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_LegacyWorkOrder.LakeView_Request_RadioReader, Database.Client, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_LegacyWorkOrder.LakeView_Tamper_Meter, Database.Client, null, true);
                }

                return m_pDataSet_LegacyWorkOrder;
            }
        }

        // FirasI 09/06/04 Moved from Common_Platform_Data

        public DataSet_Outage Outage
        {
            get
            {
                if (m_pDataSet_Outage == null)
                {
                    m_pDataSet_Outage = new DataSet_Outage();

                    // SystemEngine.StorageEngine.SetDataSet(m_pDataSet_Outage);

                    if (!Utility_ID.UseUniqueIdentifiers)
                    {
                        // MAL 15.11.27 : skip fill of outage tables for new schema ( filled in Outage code )
                        // MX: 09.09.17: pre-fill some outage tables

                        SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Outage.Outage_Attribute_Crew, Database.SpatialV1, null, true);
                        SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Outage.Outage_Attribute_EventType, Database.SpatialV1, null, true);
                        SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Outage.Outage_Attribute_EventStatus, Database.SpatialV1, null, true);
                        SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Outage.Outage_Attribute_Code, Database.SpatialV1, null, true);
                        SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Outage.Outage_Attribute_CodeType, Database.SpatialV1, null, true);
                        SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Outage.Outage_Attribute_CodeCategory, Database.SpatialV1, null, true);
                        SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Outage.Outage_Attribute_TimeStampType, Database.SpatialV1, null, true);
                        SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Outage.Outage_EventType_Notification, Database.SpatialV1, null, true);
                    }
                }

                return m_pDataSet_Outage;
            }
        }

        public DataSet_LegacyInventory LegacyInventory
        {
            get
            {
                if (m_pDataSet_LegacyInventory == null)
                {
                    m_pDataSet_LegacyInventory = new DataSet_LegacyInventory();

                    // SystemEngine.StorageEngine.SetDataSet(m_pDataSet_LegacyInventory);
                }

                return m_pDataSet_LegacyInventory;
            }
        }
        public DataSet_JointUse JointUse
        {
            get
            {
                if (m_pDataSet_JointUse == null)
                {
                    m_pDataSet_JointUse = new DataSet_JointUse();

                    // SystemEngine.StorageEngine.SetDataSet(m_pDataSet_JointUse);
                }
                return m_pDataSet_JointUse;
            }
        }

        // FirasI 09/06/04 End

        public DataSet_Redline Redline
        {
            get
            {
                if (m_pDataSet_Redline == null)
                {
                    m_pDataSet_Redline = new DataSet_Redline();

                    //SystemEngine.StorageEngine.SetDataSet(m_pDataSet_Redline);

                    if (!Utility_ID.UseUniqueIdentifiers)
                    {
                        SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Redline.Redline_Symbol_Group, Database.FusionV1, null, true);
                        SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Redline.Redline_Symbol_Type, Database.FusionV1, null, true);
                    }

                    // SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Redline.Legacy_Query_OutageProblem, Database.Spatial, null, true);
                }

                return m_pDataSet_Redline;
            }
        }

        public DataSet_Attribute Attribute
        {
            get
            {
                if (m_pDataSet_Attribute == null)
                {
                    m_pDataSet_Attribute = new DataSet_Attribute();

                    // SystemEngine.StorageEngine.SetDataSet(m_pDataSet_Attribute);
                    Database_Command_SQL l_pCommand;

                    l_pCommand = new Database_Command_SQL(Database.SpatialV1, Database_Command.MethodType.Select, (Utility_ID.UseUniqueIdentifiers ? "Sequence As " : "") + "UniqueID,Name", (Utility_ID.UseUniqueIdentifiers ? "Legacy." : "") + m_pDataSet_Attribute.Spatial_Library_WireSizing.TableName, "", "", "");
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Attribute.Spatial_Library_WireSizing, Database.SpatialV1, l_pCommand, true);

                    l_pCommand = new Database_Command_SQL(Database.SpatialV1, Database_Command.MethodType.Select, (Utility_ID.UseUniqueIdentifiers ? "Sequence As " : "") + "UniqueID,Name", (Utility_ID.UseUniqueIdentifiers ? "Legacy." : "") + m_pDataSet_Attribute.Spatial_System_Phase.TableName, "", "", "");
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Attribute.Spatial_System_Phase, Database.SpatialV1, l_pCommand, true);

                    l_pCommand = new Database_Command_SQL(Database.SpatialV1, Database_Command.MethodType.Select, (Utility_ID.UseUniqueIdentifiers ? "Sequence As " : "") + "UniqueID,Name", (Utility_ID.UseUniqueIdentifiers ? "Legacy." : "") + m_pDataSet_Attribute.Spatial_System_WirePosition.TableName, "", "", "");
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Attribute.Spatial_System_WirePosition, Database.SpatialV1, l_pCommand, true);

                    l_pCommand = new Database_Command_SQL(Database.SpatialV1, Database_Command.MethodType.Select, (Utility_ID.UseUniqueIdentifiers ? "Sequence As " : "") + "UniqueID,Name", (Utility_ID.UseUniqueIdentifiers ? "Legacy." : "") + m_pDataSet_Attribute.Spatial_System_ConductorPosition.TableName, "", "", "");
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_Attribute.Spatial_System_ConductorPosition, Database.SpatialV1, l_pCommand, true);                    
                }

                return m_pDataSet_Attribute;
            }
        }

        public DataSet_ReportSpatial ReportSpatial
        {
            get
            {
                if (m_pDataSet_ReportSpatial == null)
                {
                    m_pDataSet_ReportSpatial = new DataSet_ReportSpatial();

                    // SystemEngine.StorageEngine.SetDataSet(m_pDataSet_ReportSpatial);
                }

                return m_pDataSet_ReportSpatial;
            }
        }

        public DataSet_ReportWorkOrder ReportWorkOrder
        {
            get
            {
                if (m_pDataSet_ReportWorkOrder == null)
                {
                    m_pDataSet_ReportWorkOrder = new DataSet_ReportWorkOrder();

                    // SystemEngine.StorageEngine.SetDataSet(m_pDataSet_ReportWorkOrder);
                }

                return m_pDataSet_ReportWorkOrder;
            }
        }

        public DataSet_SpatialData SpatialData
        {
            get
            {
                if (m_pDataSet_SpatialData == null)
                {
                    m_pDataSet_SpatialData = new DataSet_SpatialData();
                    
                    DataSet_SpatialData.StorageEngine = (Storage_Engine)SystemEngine.StorageEngine;

                    // SystemEngine.StorageEngine.SetDataSet(m_pDataSet_SpatialData);
                }

                return m_pDataSet_SpatialData;
            }
        }

        public DataSet_SpatialSnapshot Spatial_Snapshot
        {
            get
            {
                if (m_pDataSet_SpatialSnapshot == null)
                {
                    m_pDataSet_SpatialSnapshot = new DataSet_SpatialSnapshot();

                    // SystemEngine.StorageEngine.SetDataSet(m_pDataSet_SpatialSnapshot);

                    Database_Command_SQL l_pCommand;

                    l_pCommand = new Database_Command_SQL(Database.Spatial, Database_Command.MethodType.Select, (Utility_ID.UseUniqueIdentifiers ? "Sequence As " : "") + "UniqueID,Name", (Utility_ID.UseUniqueIdentifiers ? "Legacy." : "") + m_pDataSet_SpatialSnapshot.Spatial_Library_WireSizing.TableName, "", "", "");
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot.Spatial_Library_WireSizing, Database.Spatial, l_pCommand, true);

                    l_pCommand = new Database_Command_SQL(Database.Spatial, Database_Command.MethodType.Select, (Utility_ID.UseUniqueIdentifiers ? "Sequence As " : "") + "UniqueID,Name", (Utility_ID.UseUniqueIdentifiers ? "Legacy." : "") + m_pDataSet_SpatialSnapshot.Spatial_System_Phase.TableName, "", "", "");
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot.Spatial_System_Phase, Database.Spatial, l_pCommand, true);

                    l_pCommand = new Database_Command_SQL(Database.Spatial, Database_Command.MethodType.Select, (Utility_ID.UseUniqueIdentifiers ? "Sequence As " : "") + "UniqueID,Name", (Utility_ID.UseUniqueIdentifiers ? "Legacy." : "") + m_pDataSet_SpatialSnapshot.Spatial_System_WirePosition.TableName, "", "", "");
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot.Spatial_System_WirePosition, Database.Spatial, l_pCommand, true);

                    l_pCommand = new Database_Command_SQL(Database.Spatial, Database_Command.MethodType.Select, (Utility_ID.UseUniqueIdentifiers ? "Sequence As " : "") + "UniqueID,Name", (Utility_ID.UseUniqueIdentifiers ? "Legacy." : "") + m_pDataSet_SpatialSnapshot.Spatial_System_ConductorPosition.TableName, "", "", "");
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot.Spatial_System_ConductorPosition, Database.Spatial, l_pCommand, true);
                }

                return m_pDataSet_SpatialSnapshot;
            }
        }

        public DataSet_SpatialSnapshot_V2_Type2 Spatial_Snapshot_V2_Type2
        {
            get
            {
                if (m_pDataSet_SpatialSnapshot_V2_Type2 == null)
                {
                    m_pDataSet_SpatialSnapshot_V2_Type2 = new DataSet_SpatialSnapshot_V2_Type2();          
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V2_Type2.Spatial_Type_Macro, Database.SpatialV1, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V2_Type2.Spatial_Type_Micro, Database.SpatialV1, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V2_Type2.Spatial_Type_Data, Database.SpatialV1, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V2_Type2.Spatial_Query_Object, Database.SpatialV1, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V2_Type2.Spatial_Query_Type, Database.SpatialV1, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V2_Type2.Spatial_Query_SystemObject, Database.SpatialV1, null, true);

                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V2_Type2.Spatial_Object_Category, Database.SpatialV1, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V2_Type2.Spatial_Object_Macro, Database.SpatialV1, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V2_Type2.Spatial_Object_Micro, Database.SpatialV1, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V2_Type2.Spatial_Object_Data, Database.SpatialV1, null, true);
                }

                return m_pDataSet_SpatialSnapshot_V2_Type2;
            }
        }

        public DataSet_FusionSpatial Spatial_Snapshot_V3_Type2
        {
            get
            {
                if (m_pDataSet_SpatialSnapshot_V3_Type2 == null)
                {
                    m_pDataSet_SpatialSnapshot_V3_Type2 = new DataSet_FusionSpatial();
                    //SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V3_Type2.Spatial_Type_Macro, Database.Spatial, null, true);
                    //SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V3_Type2.Spatial_Type_Micro, Database.Spatial, null, true);
                    //SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V3_Type2.Spatial_Type_Data, Database.Spatial, null, true);
                    //SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V3_Type2.Spatial_Query_Object, Database.Spatial, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V3_Type2.Spatial_Query_Type, Database.Spatial, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V3_Type2.Spatial_Query_View, Database.Spatial, null, true);
                    SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V3_Type2.Spatial_System_PointView, Database.Spatial, null, true);
                    //SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V3_Type2.Spatial_Query_SystemObject, Database.Spatial, null, true);

                    //SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V3_Type2.Spatial_Object_Category, Database.Spatial, null, true);
                    //SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V3_Type2.Spatial_Object_Macro, Database.Spatial, null, true);
                    //SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V3_Type2.Spatial_Object_Micro, Database.Spatial, null, true);
                    //SystemEngine.StorageEngine.FillDataTable(m_pDataSet_SpatialSnapshot_V3_Type2.Spatial_Object_Data, Database.Spatial, null, true);
                }

                return m_pDataSet_SpatialSnapshot_V3_Type2;
            }
        }

        public DataSet_ReportTimeTracker ReportTimeTracker
        {
            get
            {
                if (m_pDataSet_ReportTimeTracker == null) { m_pDataSet_ReportTimeTracker = new DataSet_ReportTimeTracker(); }

                return m_pDataSet_ReportTimeTracker;
            }
        }

        public DataSet_CubeCity CubeCity
        {
            get
            {
                if (m_pDataSet_CubeCity == null) { m_pDataSet_CubeCity = new DataSet_CubeCity(); }

                return m_pDataSet_CubeCity;
            }
        }

        public DataSet_ReportSpatialV3 DataSet_ReportSpatialV3
        {
            get
            {
                if (m_pDataSet_ReportSpatialV3 == null) { m_pDataSet_ReportSpatialV3 = new DataSet_ReportSpatialV3(); }
                return m_pDataSet_ReportSpatialV3;
            }
        }

        public DataSet_ReportOutageV3 DataSet_ReportOutageV3
        {
            get
            {
                if (m_pDataSet_ReportOutageV3 == null) { m_pDataSet_ReportOutageV3 = new DataSet_ReportOutageV3(); }
                return m_pDataSet_ReportOutageV3;
            }
        }

        public DataSet_Datapump DataSet_Datapump
        {
            get
            {
                if (m_pDataSet_Datapump == null)
                {
                    m_pDataSet_Datapump = new DataSet_Datapump();

                }

                return m_pDataSet_Datapump;
            }
        }
      
    }
}
